#include <iostream>
#include <chrono>

#include "sudok/model/board.h"

namespace
{
  void print_board(sudok::board const& board)
  {
    for (sudok::board::size_type row = 0; row < sudok::board::rows; row++)
    {
      for (sudok::board::size_type col = 0; col < sudok::board::cols; col++)
      {
        auto const val = value(board, row, col);

        if (val == 0)
        {
          std::cout << '-';
        }
        else
        {
          std::cout << val;
        }

        if (col == sudok::board::cols - 1)
        {
          continue;
        }
        else if (col % sudok::board::base == sudok::board::base - 1)
        {
          std::cout << "   ";
        }
        else
        {
          std::cout << ", ";
        }
      }

      std::cout << std::endl;
      if (row % sudok::board::base == sudok::board::base - 1)
      {
        std::cout << std::endl;
      }
    }
  }
  sudok::board make_board()
  {
    sudok::board board;

    set(board, 0, 0, 1);
    set(board, 0, 1, 7);
    set(board, 1, 7, 5);
    set(board, 2, 0, 3);
    set(board, 2, 2, 6);
    set(board, 2, 8, 7);
    set(board, 3, 1, 1);
    set(board, 3, 3, 3);
    set(board, 3, 6, 4);
    set(board, 3, 7, 7);
    set(board, 4, 3, 1);
    set(board, 4, 5, 4);
    set(board, 4, 6, 6);
    set(board, 5, 0, 9);
    set(board, 5, 3, 8);
    set(board, 5, 4, 7);
    set(board, 5, 7, 3);
    set(board, 6, 2, 9);
    set(board, 6, 3, 5);
    set(board, 6, 4, 3);
    set(board, 6, 7, 1);
    set(board, 6, 8, 6);
    set(board, 7, 1, 5);
    set(board, 7, 7, 4);
    set(board, 7, 8, 3);
    set(board, 8, 0, 7);
    set(board, 8, 4, 9);
    set(board, 8, 6, 5);

    return board;
  }

  class accumulate_check
  {
  public:
    bool check(sudok::board::size_type value)
    {
      if (value == sudok::board::empty)
      {
        return true;
      }

      if (m_used[value - 1])
      {
        return false;
      }

      m_used[value - 1] = true;
      return true;
    }
  private:
    bool m_used[sudok::board::max_value] = {};
  };
  bool row_valid(sudok::board const& board, sudok::board::size_type row)
  {
    accumulate_check checker;
    for (sudok::board::size_type col = 0; col < sudok::board::cols; col++)
    {
      if (!checker.check(sudok::value(board, row, col)))
      {
        return false;
      }
    }
    return true;
  }
  bool rows_valid(sudok::board const& board)
  {
    for (sudok::board::size_type row = 0; row < sudok::board::rows; row++)
    {
      if (!row_valid(board, row))
      {
        return false;
      }
    }
    return true;
  }
  bool col_valid(sudok::board const& board, sudok::board::size_type col)
  {
    accumulate_check checker;
    for (sudok::board::size_type row = 0; row < sudok::board::rows; row++)
    {
      if (!checker.check(sudok::value(board, row, col)))
      {
        return false;
      }
    }
    return true;
  }
  bool cols_valid(sudok::board const& board)
  {
    for (sudok::board::size_type col = 0; col < sudok::board::cols; col++)
    {
      if (!col_valid(board, col))
      {
        return false;
      }
    }
    return true;
  }
  bool subcell_valid(sudok::board const& board, sudok::board::size_type subcell_row, sudok::board::size_type subcell_col)
  {
    sudok::board::size_type const first_row = sudok::board::base * subcell_row;
    sudok::board::size_type const first_col = sudok::board::base * subcell_col;

    accumulate_check checker;

    for (sudok::board::size_type row_offset = 0; row_offset < sudok::board::base; row_offset++)
    {
      for (sudok::board::size_type col_offset = 0; col_offset < sudok::board::base; col_offset++)
      {
        sudok::board::size_type const row = first_row + row_offset;
        sudok::board::size_type const col = first_col + col_offset;
        if (!checker.check(sudok::value(board, row, col)))
        {
          return false;
        }
      }
    }
    return true;
  }
  bool subcells_valid(sudok::board const& board)
  {
    for (sudok::board::size_type subcell_row = 0; subcell_row < sudok::board::base; subcell_row++)
    {
      for (sudok::board::size_type subcell_col = 0; subcell_col < sudok::board::base; subcell_col++)
      {
        if (!subcell_valid(board, subcell_row, subcell_col))
        {
          return false;
        }
      }
    }
    return true;
  }
  bool board_valid(sudok::board const& board)
  {
    return rows_valid(board) && cols_valid(board) && subcells_valid(board);
  }
  bool full(sudok::board const& board)
  {
    for (sudok::board::size_type row = 0; row < sudok::board::rows; row++)
    {
      for (sudok::board::size_type col = 0; col < sudok::board::cols; col++)
      {
        if (value(board, row, col) == sudok::board::empty)
        {
          return false;
        }
      }
    }
    return true;
  }
  bool solve(sudok::board& board)
  {
    if (full(board)) //TODO: Make a smarter exit condition that doesn't require so much iteration
    {
      return true;
    }

    for (sudok::board::size_type row = 0; row < sudok::board::rows; row++)
    {
      for (sudok::board::size_type col = 0; col < sudok::board::cols; col++)
      {
        if (value(board, row, col) != sudok::board::empty)
        {
          continue;
        }

        for (uint16_t trial_value = 1; trial_value <= sudok::board::max_value; trial_value++)
        {
          set(board, row, col, trial_value);
          if (board_valid(board) && solve(board))
          {
            return true;
          }
          set(board, row, col, sudok::board::empty);
        }
        
        //none of the trial values worked
        return false;
      }
    }
    return false;
  }
}

int main(int, char**)
{
  sudok::board board = make_board();

  std::cout << "Initial:" << std::endl;
  print_board(board);

  auto begin = std::chrono::high_resolution_clock::now();  
  bool solved = solve(board);
  auto end = std::chrono::high_resolution_clock::now();

  if (solved)
  {
    std::cout << "Solved (" << std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count() << " ms)" << std::endl;
    print_board(board);
  }
  else
  {
    std::cout << "Failed to solve!" << std::endl;
  }

  getchar();
  return 0;
}
