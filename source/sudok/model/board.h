#pragma once

#include <cstdint>

namespace sudok
{
  struct board
  {
    typedef uint16_t size_type;
    static const size_type base = 3;
    static const size_type max_value = base * base;
    static const size_type rows = max_value;
    static const size_type cols = max_value;
    static const size_type empty = 0;

    size_type m_board[rows][cols] = {};
  };

  board::size_type value(board const& board, board::size_type row, board::size_type col);
  void set(board& board, board::size_type row, board::size_type col, board::size_type value);
}