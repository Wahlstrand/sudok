#include "sudok/model/board.h"

sudok::board::size_type sudok::value(board const& board, board::size_type row, board::size_type col)
{
  return board.m_board[row][col];
}
void sudok::set(board& board, board::size_type row, board::size_type col, board::size_type value)
{
  board.m_board[row][col] = value;
}